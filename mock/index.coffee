express = require('express')
fs = require 'fs'
app = express()
requests = {}

app.get '/', (req, res) ->
  res.send 'End Point Test Server!'
  return

app.get '/automationrequests/status/:requestId' , (req, res) ->
	debugger;
	response = {outcome:"SUCCESS", status:"PENDING"}
	id = parseInt(req.params.requestId)
	requests[id] += 1

	if(requests[id] > 4)
		response.status = "COMPLETE"

	response.count = requests[id] 

	res.send(response)
	return

app.put '/automationrequests/status/' , (req, res) ->
	res.send "ok"
	return
 
app.post '/automationrequests/' , (req, res) ->
	id =  Math.floor(Math.random() * 1000) 
	
	requests[id] = 0
	res.send { requestId: id}
	return

server = app.listen(8081, ->
  host = server.address().address
  port = server.address().port
  console.log 'Mock Target Server listening at http://%s:%s', host, port
  return
)

