"use strict";

  var httpSync = require('http-sync');
  var bunyan = require('bunyan');
  var log;

  var PENDING = 1;
  var COMPLETE = -1;

  exports.queueName = 'service requests';

  // Logging Setup
  exports.initLogging = function(name) {
    log = bunyan.createLogger({
      name: name,
      streams: [{
        level: 'info',
        stream: process.stdout // log INFO and above to stdout
      }, {
        level: 'debug',
        stream: process.stdout // log DEBUG and above to stdout
      }, {
        level: 'error',
        path: 'logs/connector.log' // log ERROR and above to a file
      }]
    });
    return log;
  };

	exports.sendSyncRequest = function(request) {
		var request = httpSync.request(request);

		var timedout = false;
		request.setTimeout(10, function() {
		    log.warn("Request Timedout!");
		    timedout = true;
		});
		var response = request.end();
		return response;
	};

	exports.sendRequest = function(sreq) {
		var request = {
			method: 'POST',
			headers: {},
			body: sreq,

			protocol: 'http',
			host: '127.0.0.1',
			port: 8081, //443 if protocol = https
			path: '/automationrequests/'
		};

		var response = this.sendSyncRequest(request);
		log.debug("SEND : " + response.body.toString());
		return JSON.parse(response.body.toString());
	};

	exports.statusRequest = function(reqId) {
			var request = {
			method: 'GET',
			headers: {},
			protocol: 'http',
			host: '127.0.0.1',
			port: 8081, //443 if protocol = https
			path: '/automationrequests/status/' + reqId
		};

		var response = this.sendSyncRequest(request);
		log.debug("STATUS : " + response.body.toString());
		return JSON.parse(response.body.toString());
	};

/*
	function closeRBARequest(reqId) {
	}

	function updateECCStatus(reqId) {
	}
*/

