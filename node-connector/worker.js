"use strict";

var Queue = require('bull');
var common = require('./common');

var log = common.initLogging("worker");

var requestQueue = Queue(common.queueName, 6379, '127.0.0.1');

//
// Queue Processing
//

function getStatus(reqId) {
  setTimeout(function() {
      console.log("");
  }, 10);

  var response = common.statusRequest(reqId);
  log.debug("status :" + response.status);

  if(response.status == 'PENDING') {
    return common.PENDING;
  } else if(response.status == 'COMPLETE') {
    return common.COMPLETE;
  }
};

requestQueue.process(function(job) {
  log.info('RequestId ' + job.data.requestId + ' entered queue!');

  var rVal = 0;
  do {
   rVal = getStatus(job.data.requestId);;
  } while (rVal != common.COMPLETE);

  if(rVal == -1) {
      return Promise.resolve();
  }
});

requestQueue.on('resumed', function(job){
  log.debug('resumed');
  return;
})

requestQueue.on('paused', function(){
  log.debug('paused');
  return;
})

requestQueue.on('ready', function() {
  log.debug('ready');
  return;
});

requestQueue.on('completed', function(job) {
  log.debug('job', job.jobId, 'completed');
  return;
});

requestQueue.on('failed', function(job, err) {
  log.debug('job', job.jobId, 'failed', err);
  return;
});

requestQueue.on('active', function(job, jobPromise){
  // Job is active
});

requestQueue.on('progress', function(job, progress){
  // Job progress updated!
});


//
// This should be activated by the main process exiting
//
process.on('SIGTERM', function () {
  requestQueue.close().then(function () {
    log.info('Closing Queue !')
  });
});
