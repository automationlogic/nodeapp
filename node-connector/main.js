"use strict";

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var bunyan = require('bunyan');
var common = require('./common');


var log = common.initLogging("connector");

// Express Hooks
app.use(bodyParser.json());

// Also, in express you can catch all errors like this
app.use(function(err, req, res, next) {
   log.error(err);
   res.send(500, 'An error occurred');
});


// Queue and toureiro Setup
var Queue = require('bull');
var toureiro = require('toureiro');
app.use('/toureiro', toureiro());

var requestQueue = Queue(common.queueName, 6379, '127.0.0.1');

// Express Application Hooks

app.get('/', function (req, res) {
  res.send('Connector : Running!');
});

app.get('/automationrequests/status/', function (req, res) {
	res.status(200);
  res.setHeader('Content-Type', 'application/json');
  var count = Promise.resolve("").then(requestQueue.count);
	var message = "Count : " + count + "\n";
	return;
});

function processRequest(item, index, array) {
	var response = common.sendRequest(item);
  var opts = {
    'attempts': 5
  };
	if(response.requestId != -1) {
		requestQueue.add(response, opts);
  }
  return response.requestId;
}

app.post('/automationrequests/', function(req, res) {
  var requests = req.body;
  var reqIds = [requests.length];
  for(var i = 0; i <= (requests.length - 1); i++) {
    reqIds[i] = processRequest(requests[i]);
  }

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify( reqIds ));
	return;
});

var server = app.listen(8079, function () {
	var host = server.address().address;
	var port = server.address().port;

	log.info("Connector App listening at http://%s:%s", host, port);
	return;
});

// Spawn worker instance
var spawn = require('child_process').spawn,
    worker  = spawn('node', ['worker.js']);

// Exit handler
process.on('SIGTERM', function () {
	server.close(function () {
    worker.kill();
		requestQueue.close().then(function () { log.info('Closing Queue !') });
  });
});
